
$('input, :input').attr('autocomplete', 'off');

$(document).on('click','.delete',function(e){
  e.preventDefault();
  var CSRF_TOKEN = $('input[name="_token"]').attr('value');
  var METHOD = $('input[name="_method"]').attr('value');
  var url = $(this).closest('form').attr('action');;
  if(confirm('Are you sure want to delete ?')) {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
      url: url,
      type: 'post',
      dataType: 'json',
      data: {
          _method: 'DELETE',
          submit: true
      },
      success : function(data){
        if(data.msg) {
          // table.draw(false);
          table.ajax.reload();
          $('#alert').empty().append("<div class='alert alert-success alert-dismissible mb-2' role='alert'>"+
            "<button type='button' class='close' data-dismiss='alert' aria-label='Close'>"+
            "<span aria-hidden='true'>×</span></button><span>"+data.success+"</span></div>");
        } else {
          $('#alert').empty().append("<div class='alert alert-danger alert-dismissible mb-2' role='alert'>"+
            "<button type='button' class='close' data-dismiss='alert' aria-label='Close'>"+
            "<span aria-hidden='true'>×</span></button><span>"+data.success+"</span></div>");
        }
      }
    }).always(function(data) {
        // $('#dataTableBuilder').DataTable().draw(false);
    });
    
  } else {

  }
});