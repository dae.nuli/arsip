<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
    // return view('welcome');
    // return view('admin.auth.login');
    // return view('auth.login');
});


Route::get('login', 'AuthStaff\LoginController@showLoginForm')->name('login');
Route::post('login', 'AuthStaff\LoginController@login');
Route::post('logout', 'AuthStaff\LoginController@logout')->name('logout');


Route::get('register', 'AuthStaff\RegisterController@showRegistrationForm')->name('register');
Route::post('register', 'AuthStaff\RegisterController@register');
Route::get('email/verify', 'AuthStaff\VerificationController@show')->name('verification.notice');
Route::get('email/verify/{id}', 'AuthStaff\VerificationController@verify')->name('verification.verify');
Route::get('email/resend', 'AuthStaff\VerificationController@resend')->name('verification.resend');

Route::name('staff.')->group(function () {
	Route::group(['middleware' => ['staff', 'auth:staff']], function () {
		Route::get('home', 'AuthStaff\HomeController@index')->name('home');

		Route::middleware(['verified'])->group(function () {
			Route::get('archive', 'AuthStaff\ArchiveController@index')->name('archive.index');
			Route::get('archive/data', 'AuthStaff\ArchiveController@data')->name('archive.data');
			Route::get('archive/{id}', 'AuthStaff\ArchiveController@show')->name('archive.show');
			Route::get('archive/download/{id}', 'AuthStaff\ArchiveController@download')->name('archive.download');
		});

	});
});

// Auth::routes();

Route::prefix('admin')->group(function () {

	Route::name('admin.')->group(function () {

		Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
		Route::post('login', 'Auth\LoginController@login');

		Route::group(['middleware' => ['auth']], function () {


			Route::post('logout', 'Auth\LoginController@logout')->name('logout');

			Route::get('home', 'HomeController@index')->name('home');

			Route::post('incoming/upload/{id}', 'Admin\IncomingMailController@upload')->name('incoming.upload');
			Route::get('incoming/view/{id}', 'Admin\IncomingMailController@viewFile')->name('incoming.view');
			Route::post('incoming/download', 'Admin\IncomingMailController@download')->name('incoming.download');
			Route::get('incoming/print/{id}', 'Admin\IncomingMailController@print')->name('incoming.print');
			Route::get('incoming/data', 'Admin\IncomingMailController@data')->name('incoming.data');
			Route::resource('incoming', 'Admin\IncomingMailController');

			// Route::post('outgoing/upload/{id}', 'Admin\OutgoingMailController@upload')->name('outgoing.upload');
			// Route::get('outgoing/view/{id}', 'Admin\OutgoingMailController@viewFile')->name('outgoing.view');
			// Route::post('outgoing/download', 'Admin\OutgoingMailController@download')->name('outgoing.download');
			// Route::get('outgoing/print/{id}', 'Admin\OutgoingMailController@print')->name('outgoing.print');
			// Route::get('outgoing/data', 'Admin\OutgoingMailController@data')->name('outgoing.data');
			// Route::resource('outgoing', 'Admin\OutgoingMailController');

			// Route::get('archive/download/{id}', 'Admin\ArchiveController@download')->name('archive.download');
			// Route::get('archive/data', 'Admin\ArchiveController@data')->name('archive.data');
			// Route::resource('archive', 'Admin\ArchiveController');
      Route::get('profile', 'Admin\ProfileController@index')->name('profile.index');
      Route::post('profile/update', 'Admin\ProfileController@update')->name('profile.update');
      Route::post('profile/updatePass', 'Admin\ProfileController@updatePass')->name('profile.pass');

      Route::group(['middleware' => ['checkRole']], function () {

  			Route::get('category/data', 'Admin\CategoryController@data')->name('category.data');
  			Route::resource('category', 'Admin\CategoryController');

  			Route::post('staff/deleteAll', 'Admin\StaffController@postDeleteAll')->name('staff.delete.all');
  			Route::get('staff/data', 'Admin\StaffController@data')->name('staff.data');
  			Route::resource('staff', 'Admin\StaffController');

  			Route::post('user/deleteAll', 'Admin\AdminController@postDeleteAll')->name('user.delete.all');
  			Route::get('user/data', 'Admin\AdminController@data')->name('user.data');
  			Route::resource('user', 'Admin\AdminController');

      });

		});

	});

});
