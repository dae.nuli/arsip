<?php

namespace App\Http\Controllers\AuthStaff;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Http\Controllers\Controller;
use App\Models\IncomingMail;
use App\Models\Archive;
use App\Models\Category;
use DataTables;
use Form;
use File;
use Carbon\Carbon;

class ArchiveController extends Controller
{
	public $uri = 'staff.archive';

    public function index()
    {
    	// $data['archive'] = Archive::orderBy('category_id')->get();
    	$data['ajax'] = route($this->uri.'.data');
    	return view('staff.archive.index', $data);
    }

    public function data(Request $request)
    {
        if ($request->ajax()) {
            $data = IncomingMail::select(['id', 'type', 'category_id', 'files','disposition', 'letter_entry']);
            return DataTables::of($data)
                ->editColumn('category_id', function ($index) {
                    return ($index->category->name) ?? '-';
                })
                ->editColumn('type', function ($index) {
									if ($index->type == 1) {
										return 'Surat Masuk';
									} elseif ($index->type == 2) {
										return 'Surat Keluar';
									} else {
										return '-';
									}
                })
                ->editColumn('disposition', function ($index) {
                    return Str::words($index->disposition, 2);
                })
                ->editColumn('letter_entry', function ($index) {
                    return Carbon::parse($index->letter_entry)->format('d-m-Y H:i');
                })
                ->addColumn('action', function ($index) {
                    $tag = !empty($index->files) ? "<a href=".route($this->uri.'.download',$index->id)." class='btn btn-success btn-sm'><i class='fa fa-download' aria-hidden='true'></i></a>" : "<a href='#' class='btn btn-success btn-sm disabled'><i class='fa fa-download' aria-hidden='true'></i></a>";
                    $tag .= " <a href=".route($this->uri.'.show',$index->id)." class='btn btn-info btn-sm'><i class='fa fa-info-circle' aria-hidden='true'></i></a>";
                    return $tag;
                })
                ->rawColumns(['id', 'action'])
                ->make(true);
        }
    }

    public function show($id)
    {
    	$data['row'] = IncomingMail::find($id);
    	return view('staff.archive.show', $data);
    }

    public function download($id)
    {
        $item = IncomingMail::find($id);
        if (!empty($item->files)) {
            if(File::exists($item->files)) {
                return response()->download($item->files);
            }
        }
        return 'File does not exist';
    }

}
