<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Kris\LaravelFormBuilder\FormBuilder;
use App\Models\Archive;
use App\Models\Category;
use DataTables;
use Form;
use File;

class ArchiveController extends Controller
{
    private $folder = 'admin.archive';
    private $uri = 'admin.archive';
    private $title = 'Archive';
    private $desc = 'Description';

    public function __construct(Archive $table)
    {
    	$this->table = $table;
    }

    public function index(Request $request)
    {
        $data['title'] = $this->title;
        $data['ajax'] = route($this->uri.'.data');
        $data['create'] = route($this->uri.'.create');
        return view($this->folder.'.index',$data);
    }

    public function data(Request $request)
    {
        if ($request->ajax()) {
            $data = $this->table->select(['id', 'category_id', 'title', 'content', 'created_at']);
            return DataTables::of($data)
                ->editColumn('category_id', function ($index) {
                    return ($index->category->name) ?? '-';
                })
                // ->editColumn('content', function ($index) {
                //     return str_limit($index->content, 10);
                // })
                ->addColumn('action', function ($index) {
                    $tag = Form::open(array("url" => route($this->uri.'.destroy',$index->id), "method" => "DELETE"));
                    $tag .= "<a href=".route($this->uri.'.edit',$index->id)." class='btn btn-primary btn-xs'><i class='fa fa-pencil'></i> Edit</a>";
                    $tag .= " <a href=".route($this->uri.'.show',$index->id)." class='btn btn-success btn-xs'><i class='fa fa-info'></i> Show</a>";
                    $tag .= " <button type='submit' class='delete btn btn-danger btn-xs'><i class='fa fa-trash'></i> Delete</button>";
                    $tag .= Form::close();
                    return $tag;
                })
                ->rawColumns(['id', 'action'])
                ->make(true);
        }
    }

    public function create(FormBuilder $formBuilder)
    {
        $data['title'] = $this->title;
        $data['form'] = $formBuilder->create('App\Forms\ArchiveForm', [
            'method' => 'POST',
            'url' => route($this->uri.'.store')
        ]);
        $data['url'] = route($this->uri.'.index');
        return view($this->folder.'.create', $data);
    }

    public function edit(FormBuilder $formBuilder, $id)
    {
        $data['title'] = $this->title;
        $tbl = $this->table->find($id);
        $data['form'] = $formBuilder->create('App\Forms\ArchiveForm', [
            'method' => 'PUT',
            'model' => $tbl,
            'url' => route($this->uri.'.update', $id)
        ])
        ->modify('files', 'static', [
        // ->addAfter('attachment', 'bayar', 'static', [
            'template' => 'admin.archive.edit_file',
            'value' => function ($val) use ($tbl) {
                return $tbl;
            }
            // 'value' => function ($val) {
            //     return $val;
            // }
            // 'value' => $this->getFieldValues()
            // 'value' => function ($val) {
            //     // $tp = TopUp::find($id);
            //     return $val->topup_date;
            //     // return '<img id="blah" src="'.asset('topupattachment/'.$val).'" alt="" width="200" />';
            // }
            //     return !empty($val) ? ;
            // }
        ]);

        $data['url'] = route($this->uri.'.index');
        return view($this->folder.'.create', $data);
    }

    public function show(FormBuilder $formBuilder, $id)
    {
        $data['title'] = $this->title;
        $tbl = $this->table->find($id);
        $data['form'] = $formBuilder->create('App\Forms\ArchiveForm', [
            'method' => 'PUT',
            'model' => $tbl,
            'url' => route($this->uri.'.update', $id)
        ])
        ->modify('category_id', 'select', [
            'attr' => [
                'disabled' => '',
            ]
        ])
        ->modify('title', 'text', [
            'attr' => [
                'disabled' => '',
            ]
        ])
        ->modify('content', 'textarea', [
            'attr' => [
                'disabled' => '',
            ]
        ])
        ->modify('files', 'static', [
        // ->addAfter('attachment', 'bayar', 'static', [
            'template' => 'admin.archive.show_file',
            'value' => function ($val) use ($tbl) {
                return $tbl;
            }
            // 'value' => $this->getFieldValues()
            // 'value' => function ($val) {
            //     // $tp = TopUp::find($id);
            //     return $val->topup_date;
            //     // return '<img id="blah" src="'.asset('topupattachment/'.$val).'" alt="" width="200" />';
            // }
            //     return !empty($val) ? ;
            // }
        ]);

        $data['url'] = route($this->uri.'.index');
        return view($this->folder.'.show', $data);
    }

    public function store(Request $request)
    {
        $this->table->create($request->all());
        return redirect(route($this->uri.'.index'))->with('success',trans('message.create'));
    }

    public function update(Request $request, $id)
    {
        $this->table->findOrFail($id)->update($request->all());
        return redirect(route($this->uri.'.index'))->with('success', trans('message.update'));
    }
    
    public function destroy($id)
    {
        $tb = $this->table->findOrFail($id);
        $tb->delete();
        return response()->json(['msg' => true,'success' => trans('message.delete')]);
    }

    public function download($id)
    {
        $item = Archive::find($id);
        if (!empty($item->files)) {
            if(File::exists($item->files)) {
                return response()->file($item->files);
            }
        }
        return 'File does not exist';
    }
}
