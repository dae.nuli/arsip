<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\User;

class ProfileController extends Controller
{
    private $folder = 'admin.profile';
    private $uri = 'admin.profile';
    private $title = 'User Profile';
    private $desc = 'Description';

    public function __construct(User $table)
    {
    	$this->table = $table;
    }

    public function index(Request $request)
    {
      $data['row'] = $this->table->findOrFail(Auth::user()->id);
        $data['title'] = $this->title;
        $data['update'] = route($this->uri.'.update');
        $data['updatePass'] = route($this->uri.'.pass');
        return view($this->folder.'.index',$data);
    }

    public function update(Request $request)
    {
      $request->validate([
        'name' => 'required|string|max:255'
      ]);
      $this->table->findOrFail(Auth::user()->id)
                  ->update([
                    'name' => $request->name
                  ]);
      return redirect()->back()->with('success', 'Profile has been changed');
    }

    public function updatePass(Request $request)
    {
      $request->validate([
          'password' => 'required|string|min:6|confirmed'
      ]);
      $this->table->findOrFail(Auth::user()->id)
            ->update([
              'password' => bcrypt($request->password)
            ]);
      return redirect()->back()->with('success', 'Password has been changed');
    }
    //
}
