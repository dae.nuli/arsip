<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Kris\LaravelFormBuilder\FormBuilder;
use App\Models\OutgoingMail;
use DataTables;
use Form;
use Carbon\Carbon;
use PDF;
use File;

class OutgoingMailController extends Controller
{
    private $folder = 'admin.outgoing_mail';
    private $uri = 'admin.outgoing';
    private $title = 'Outgoing Mail';
    private $desc = 'Description';

    public function __construct(OutgoingMail $table)
    {
    	$this->table = $table;
    }

    public function index(Request $request)
    {
        $data['title'] = $this->title;
        $data['ajax'] = route($this->uri.'.data');
        $data['create'] = route($this->uri.'.create');
        return view($this->folder.'.index',$data);
    }

    public function data(Request $request)
    {
        if ($request->ajax()) {
            $data = $this->table->select([
                'id', 'code', 'mail_number', 'destination_address', 'receiver', 
                'letter_entry', 'received_date', 'created_at'
            ]);
            return DataTables::of($data)
                ->editColumn('letter_entry', function ($index) {
                    return Carbon::parse($index->letter_entry)->format('d M Y H:i');
                })
                ->editColumn('received_date', function ($index) {
                    return ($index->received_date) ? Carbon::parse($index->received_date)->format('d M Y H:i') : '-';
                })
                ->addColumn('action', function ($index) {
                    $tag = Form::open(array("url" => route($this->uri.'.destroy',$index->id), "method" => "DELETE"));
                    $tag .= "<a href=".route($this->uri.'.edit',$index->id)." class='btn btn-primary btn-xs'><i class='fa fa-pencil'></i> Edit</a>";
                    $tag .= " <a href=".route($this->uri.'.show',$index->id)." class='btn btn-success btn-xs'><i class='fa fa-info'></i> Show</a>";
                    $tag .= " <a href=".route($this->uri.'.print',$index->id)." class='btn btn-info btn-xs'><i class='fa fa-print'></i> Print</a>";
                    $tag .= " <button type='submit' class='delete btn btn-danger btn-xs'><i class='fa fa-trash'></i> Delete</button>";
                    $tag .= Form::close();
                    return $tag;
                })
                ->rawColumns(['id', 'action'])
                ->make(true);
        }
    }

    public function create(FormBuilder $formBuilder)
    {
        $data['title'] = $this->title;
        $data['form'] = $formBuilder->create('App\Forms\OutgoingMailForm', [
            'method' => 'POST',
            'url' => route($this->uri.'.store')
        ])
        ->remove('files');
        // ->remove('received_date');
        $data['url'] = route($this->uri.'.index');
        return view($this->folder.'.create', $data);
    }

    public function edit(FormBuilder $formBuilder, $id)
    {
        $data['title'] = $this->title;
        $tbl = $this->table->find($id);
        $data['form'] = $formBuilder->create('App\Forms\OutgoingMailForm', [
            'method' => 'PUT',
            'model' => $tbl,
            'url' => route($this->uri.'.update', $id)
        ])
        ->remove('files')
        ->modify('letter_entry', 'text', [
            'attr' => [
                'disabled' => '',
            ]
        ]);

        $data['url'] = route($this->uri.'.index');
        return view($this->folder.'.create', $data);
    }

    public function store(Request $request)
    {
        $this->table->create($request->all());
        return redirect(route($this->uri.'.index'))->with('success',trans('message.create'));
    }

    public function update(Request $request, $id)
    {
        $this->table->findOrFail($id)->update($request->all());
        return redirect(route($this->uri.'.index'))->with('success', trans('message.update'));
    }

    public function show(FormBuilder $formBuilder, $id)
    {
        $data['title'] = $this->title;
        $tbl = $this->table->find($id);
        $data['form'] = $formBuilder->create('App\Forms\OutgoingMailForm', [
            'method' => 'POST',
            'model' => $tbl,
            'url' => route($this->uri.'.upload', $id)
        ])
        ->modify('code', 'text', [
            'attr' => [
                'disabled' => '',
            ]
        ])
        ->modify('mail_number', 'text', [
            'attr' => [
                'disabled' => '',
            ]
        ])
        ->modify('subject', 'text', [
            'attr' => [
                'disabled' => '',
            ]
        ])
        ->modify('destination_address', 'text', [
            'attr' => [
                'disabled' => '',
            ]
        ])
        ->modify('receiver', 'text', [
            'attr' => [
                'disabled' => '',
            ]
        ])
        ->modify('information', 'text', [
            'attr' => [
                'disabled' => '',
            ]
        ])
        ->modify('letter_entry', 'text', [
            'attr' => [
                'disabled' => '',
            ]
        ])
        ->modify('received_date', 'text', [
            'attr' => [
                'disabled' => '',
            ]
        ])
        ->modify('files', 'static', [
        // ->addAfter('attachment', 'bayar', 'static', [
            'template' => $this->folder.'.show_file',
            'value' => function ($val) use ($tbl) {
                return $tbl;
            }
            // 'value' => $this->getFieldValues()
            // 'value' => function ($val) {
            //     // $tp = TopUp::find($id);
            //     return $val->topup_date;
            //     // return '<img id="blah" src="'.asset('topupattachment/'.$val).'" alt="" width="200" />';
            // }
            //     return !empty($val) ? ;
            // }
        ]);

        $data['url'] = route($this->uri.'.index');
        return view($this->folder.'.show', $data);
    }

    public function viewFile($id)
    {
        $item = $this->table->findOrFail($id);
        if (!empty($item->files)) {
            if(File::exists($item->files)) {
                return response()->file($item->files);
            }
        }
        return 'File does not exist';
    }

    public function upload(Request $request, $id)
    {
        // dd($request->files_in);
        if (!empty($request->files_in)) {
            $this->table->findOrFail($id)->update(['files' => $request->files_in]);
            return redirect()->back()->with('success', trans('message.upload'));
        }
    }
    
    public function destroy($id)
    {
        $tb = $this->table->findOrFail($id);
        $tb->delete();
    }

    public function download(Request $request)
    {
        if (!empty($request->letter_entry)) {
            $pisah = explode(' - ', $request->letter_entry);
            $from = Carbon::parse($pisah[0])->format('Y-m-d H:i:s');
            $to = Carbon::parse($pisah[1])->format('Y-m-d').' 24:60:60';
            // dd($pisah);
            $data['type'] = 1;
            $data['data'] = $this->table->where('letter_entry', '>=', $from)->where('letter_entry', '<=', $to)->get();
            $pdf = PDF::loadView($this->folder.'.download_pdf', $data);
            return $pdf->setPaper('a4', 'landscape')->download(time().'.pdf');
        }
    }

    public function print($id)
    {
        $data['type'] = 0;
        $data['data'] = $this->table->findOrFail($id);
        $pdf = PDF::loadView($this->folder.'.download_pdf', $data);
        return $pdf->setPaper('a4', 'landscape')->stream(time().'.pdf');
    }
}
