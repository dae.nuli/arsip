<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Kris\LaravelFormBuilder\FormBuilder;
use App\Models\Staff;
use DataTables;
use Form;

class StaffController extends Controller
{
    private $folder = 'admin.staff';
    private $uri = 'admin.staff';
    private $title = 'Staff';
    private $desc = 'Description';

    public function __construct(Staff $table)
    {
    	$this->table = $table;
    }

    public function index(Request $request)
    {
        $data['title'] = $this->title;
        $data['ajax'] = route($this->uri.'.data');
        $data['create'] = route($this->uri.'.create');
        $data['action'] = route($this->uri.'.delete.all');
        return view($this->folder.'.index',$data);
    }

    public function data(Request $request)
    {
        if ($request->ajax()) {
            $data = $this->table->select(['id', 'name', 'email', 'phone', 'status', 'created_at']);
            return DataTables::of($data)
                ->editColumn('id','<input type="checkbox" class="checkbox" name="id[]" value="{{$id}}"/>')
                ->addColumn('status', function ($index) {
                    return ($index->status) ? '<div class="label label-success">ACTIVE</div>' : '<div class="label label-danger">NOT ACTIVE</div>';
                })
                ->addColumn('action', function ($index) {
                    $tag = Form::open(array("url" => route($this->uri.'.destroy',$index->id), "method" => "DELETE"));
                    $tag .= "<a href=".route($this->uri.'.edit',$index->id)." class='btn btn-primary btn-xs'><i class='fa fa-pencil'></i> Edit</a>";
                    // $tag .= " <a href=".route($this->uri.'.show',$index->id)." class='btn btn-success btn-xs'>Show</a>";
                    $tag .= " <button type='submit' class='delete btn btn-danger btn-xs'><i class='fa fa-trash'></i> Delete</button>";
                    $tag .= Form::close();
                    return $tag;
                })
                ->rawColumns(['id', 'status', 'action'])
                ->make(true);
        }
    }

    public function create(FormBuilder $formBuilder)
    {
        $data['title'] = $this->title;
        $data['form'] = $formBuilder->create('App\Forms\StaffForm', [
            'method' => 'POST',
            'url' => route($this->uri.'.store')
        ]);
        $data['url'] = route($this->uri.'.index');
        return view($this->folder.'.create', $data);
    }

    public function edit(FormBuilder $formBuilder, $id)
    {
        $data['title'] = $this->title;
        $tbl = $this->table->find($id);
        $data['form'] = $formBuilder->create('App\Forms\StaffForm', [
            'method' => 'PUT',
            'model' => $tbl,
            'url' => route($this->uri.'.update', $id)
        ])
        ->modify('status', 'choice', [
            'selected' => null
        ])
        ->modify('password', 'password', [
            'attr' => [
              'data-validation' => null,
            ],
            'value' => ''
        ]);

        $data['url'] = route($this->uri.'.index');
        return view($this->folder.'.create', $data);
    }

    public function store(Request $request)
    {
    	$request->validate([
            'email' => 'required|string|email|max:255|unique:staff'
    	]);

        $this->table->create($request->all());
        return redirect(route($this->uri.'.index'))->with('success',trans('message.create'));
    }

    public function update(Request $request, $id)
    {
    	$request->validate([
            'email' => 'required|string|email|max:255|unique:staff,email,'.$id
    	]);
    	if(empty($request->password)){
            unset($request['password']);
        }
        $this->table->findOrFail($id)->update($request->all());
        return redirect(route($this->uri.'.index'))->with('success', trans('message.update'));
    }

    public function destroy($id)
    {
        $tb = $this->table->findOrFail($id);
        $tb->delete();
        return response()->json(['msg' => true,'success' => trans('message.delete')]);
    }

    public function postDeleteAll(Request $request)
    {
        $ID = $request->id;
        if(count($ID)>0){
            foreach ($ID as $key => $value) {
                $del = $this->tabel->findOrFail($value);
                $del->delete();
            }
            return redirect(route($this->uri.'.index'))->with('success',trans('message.delete.all'));
        }
    }
}
