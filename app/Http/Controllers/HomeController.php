<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\IncomingMail;
use App\Models\OutgoingMail;
use App\Models\Category;
use App\Models\Archive;
use App\Models\Staff;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['title'] = 'Dashboard';
        $data['category'] = Category::count();
        $data['archive'] = Archive::count();
        $data['incoming'] = IncomingMail::count();
        $data['outgoing'] = OutgoingMail::count();
        $data['staff'] = Staff::count();
        return view('admin.home.index', $data);
    }
}
