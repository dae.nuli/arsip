<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = ['name'];

    public function archive()
    {
    	return $this->hasMany(Archive::class, 'category_id');
    }
}
