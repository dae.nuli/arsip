<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OutgoingMail extends Model
{
    protected $fillable = [
    	'code', 'mail_number', 'destination_address', 'files', 'subject', 'receiver', 'information', 'letter_entry', 'received_date'
    ];
}