<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Archive extends Model
{
    protected $fillable = ['category_id', 'user_id', 'title', 'content', 'files'];

    public function category()
    {
    	return $this->belongsTo(Category::class);
    }

    protected static function boot()
    {
        parent::boot();
        // static::saving(function ($model) {
        //     foreach ($model->attributes as $key => $value) {
        //         $model->{$key} = (empty($value) && $value != 0) ? null : $value;
        //     }
        // });

        static::creating(function ($model) {
        	$model->user_id = Auth::user()->id;
        	// $model->file = Auth::user()->name;
        });

        // static::updating(function ($model) {
        // 	$model->updated_by = Auth::user()->name;
        // });
    }
}
