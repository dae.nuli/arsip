<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

class IncomingMail extends Model
{
    protected $fillable = [
    	'type', 'category_id', 'code', 'mail_number', 'return_address', 'files', 'subject',
      'receiver', 'disposition', 'letter_entry', 'received_date', 'user_id'
    ];

    protected static function boot()
    {
        parent::boot();
        // static::saving(function ($model) {
        //     foreach ($model->attributes as $key => $value) {
        //         $model->{$key} = (empty($value) && $value != 0) ? null : $value;
        //     }
        // });

        static::creating(function ($model) {
        	// $model->letter_entry = Carbon::parse($model->attributes['letter_entry'])->format('Y-m-d H:i:s');
          // $model->file = Auth::user()->name;
        	$model->user_id = Auth::user()->id;
        });

        // static::updating(function ($model) {
        // 	$model->updated_by = Auth::user()->name;
        // });
    }

    public function category()
    {
      return $this->belongsTo(Category::class);
    }

    public function user()
    {
      return $this->belongsTo(App\User::class);
    }
}
