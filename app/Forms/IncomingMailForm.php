<?php

namespace App\Forms;

use Kris\LaravelFormBuilder\Form;
use App\Models\Category;

class IncomingMailForm extends Form
{
    public function buildForm()
    {
        $this
            ->add('type', 'select', [
                'choices' => [1 => 'Surat Masuk', 2 => 'Surat Keluar'],
                'selected' => 1,
                // 'empty_value' => '- Please Select -',
                'attr' => [
                    'data-validation' => 'required',
                ]
            ])
            ->add('category_id', 'select', [
                'choices' => Category::pluck('name', 'id')->toArray(),
                'empty_value' => '- Please Select -',
                'label' => 'Kategori',
                'attr' => [
                    'data-validation' => 'required',
                    'class' => 'select2 form-control',
                ]
            ])
            ->add('code', 'text', [
                'label' => 'No Berkas',
                'attr' => [
                    'data-validation' => 'required',
                ]
            ])
            ->add('mail_number', 'text', [
                'label' => 'No Surat Masuk',
                'attr' => [
                    'data-validation' => 'required',
                ]
            ])
            ->add('subject', 'text', [
                'label' => 'Perihal',
                // 'attr' => [
                //     'data-validation' => 'required',
                // ]
            ])
            ->add('return_address', 'text', [
                'label' => 'Alamat Pengirim',
                // 'attr' => [
                //     'data-validation' => 'required',
                // ]
            ])
            ->add('receiver', 'text', [
                'label' => 'Penerima',
                // 'attr' => [
                //     'required' => null,
                //     // 'data-validation-required-message' => 'This field is required'
                // ]
            ])
            ->add('disposition', 'text', [
                'label' => 'Tujuan Disposisi',
                // 'attr' => [
                //     'data-validation' => 'required',
                // ]
            ])
            ->add('letter_entry', 'text', [
                'label' => 'Tanggal Masuk',
                'attr' => [
                    'data-validation' => 'required',
                    'class' => 'datepicker form-control',
                ]
            ])
            ->add('received_date', 'text', [
                'label' => 'Tanggal Terima',
                'attr' => [
                    'required' => null,
                    'class' => 'datepicker form-control',
                ]
            ])
            ->add('files', 'file', [
                // 'template' => 'admin.archive.create_file'
                'label' => 'Berkas',
                // 'attr' => [
                //     'required' => null,
                //     'class' => 'datepicker form-control',
                // ]
                // 'attr' => ['id' => 'imgInp']
            ]);
    }
}
