<?php

namespace App\Forms;

use Kris\LaravelFormBuilder\Form;

class StaffForm extends Form
{
    public function buildForm()
    {
        $this
            ->add('name', 'text', [
                'attr' => [
                  'data-validation' => 'required',
                ]
            ])
            ->add('email', 'email', [
                'attr' => [
                  'data-validation' => 'required',
                ]
            ])
            ->add('phone', 'text')
            ->add('password', 'password', [
                'attr' => [
                  'data-validation' => 'required',
                ]
            ])
            ->add('status', 'choice', [
                'choices' => [1 => 'ACTIVE', 0 => 'NOT ACTIVE'],
                // 'choice_options' => [
                //     'wrapper' => ['class' => 'radio'],
                //     'label_attr' => ['class' => 'col-lg-10 col-md-10 col-sm-8 col-xs-7'],
                // ],
                // 'choice_options' => [
                //     'wrapper' => ['class' => 'choice-wrapper'],
                //     'label_attr' => ['class' => 'label-class'],
                // ],
                'choice_options' => [
                    'wrapper' => ['class' => 'radio status'],
                    'label_attr' => ['class' => ''],
                ],
                'selected' => [1],
                'expanded' => true,
                'multiple' => false
            ]);
    }
}
