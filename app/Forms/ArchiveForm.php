<?php

namespace App\Forms;

use Kris\LaravelFormBuilder\Form;
use App\Models\Category;

class ArchiveForm extends Form
{
    public function buildForm()
    {
        $this
            ->add('category_id', 'select', [
                'choices' => Category::pluck('name', 'id')->toArray(),
                'empty_value' => '- Please Select -',
                'label' => 'Category',
                'attr' => [
                    'data-validation' => 'required',
                    'class' => 'select2 form-control',
                ]
            ])
            ->add('title', 'text', [
                'attr' => [
                    'data-validation' => 'required',
                ]
            ])
            ->add('content', 'textarea', [
                'attr' => [
                    'data-validation' => 'required',
                ]
            ])
            ->add('files', 'file', [
                'template' => 'admin.archive.create_file'
                // 'label' => 'Gambar',
                // 'attr' => ['id' => 'imgInp']
            ]);
    }
}
