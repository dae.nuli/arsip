<?php

namespace App\Forms;

use Kris\LaravelFormBuilder\Form;

class OutgoingMailForm extends Form
{
    public function buildForm()
    {
        $this
            ->add('code', 'text', [
                'label' => 'No Berkas',
                'attr' => [
                    'data-validation' => 'required',
                ]
            ])
            ->add('mail_number', 'text', [
                'label' => 'No Surat',
                'attr' => [
                    'data-validation' => 'required',
                ]
            ])
            ->add('subject', 'text', [
                'label' => 'Perihal',
                'attr' => [
                    'data-validation' => 'required',
                ]
            ])
            ->add('destination_address', 'text', [
                'label' => 'Alamat Tujuan',
                'attr' => [
                    'data-validation' => 'required',
                ]
            ])
            ->add('receiver', 'text', [
                'label' => 'Penerima',
                'attr' => [
                    'required' => null,
                ]
            ])
            ->add('information', 'text', [
                'label' => 'Keterangan',
                'attr' => [
                    'data-validation' => 'required',
                ]
            ])
            ->add('letter_entry', 'text', [
                'label' => 'Tanggal Masuk',
                'attr' => [
                    'data-validation' => 'required',
                    'class' => 'datepicker form-control',
                ]
            ])
            ->add('received_date', 'text', [
                'label' => 'Tanggal Distribusi',
                'attr' => [
                    'required' => null,
                    'class' => 'datepicker form-control',
                ]
            ])
            ->add('files', 'file', [
                // 'template' => 'admin.archive.create_file'
                // 'label' => 'Gambar',
                // 'attr' => ['id' => 'imgInp']
            ]);
    }
}
