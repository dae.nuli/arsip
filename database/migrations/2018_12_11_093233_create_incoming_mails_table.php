<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIncomingMailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('incoming_mails', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('type', [1, 2]);
            $table->integer('category_id');
            $table->string('code');
            $table->string('mail_number');
            $table->string('subject');
            $table->string('return_address');
            $table->string('receiver')->nullable();
            $table->string('disposition');
            $table->dateTime('letter_entry')->nullable();
            $table->dateTime('received_date')->nullable();
            $table->string('files')->nullable();
            $table->integer('user_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('incoming_mails');
    }
}
