<?php

use Illuminate\Database\Seeder;
use App\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::truncate();
        User::insert([
        	[
        		'name' => 'Nuli Giarsyani',
        		'email' => 'admin@example.com',
            'password' => bcrypt(111111),
        		'status' => 1,
            'role' => 'admin',
            'created_at' => date('Y-m-d H:i:s')
        	],
          [
        		'name' => 'Ian Panrita',
        		'email' => 'ian@example.com',
            'password' => bcrypt(111111),
        		'status' => 1,
            'role' => 'user',
            'created_at' => date('Y-m-d H:i:s')
        	],
          [
        		'name' => 'Panrita',
        		'email' => 'panrita@example.com',
            'password' => bcrypt(111111),
        		'status' => 1,
            'role' => 'user',
            'created_at' => date('Y-m-d H:i:s')
        	]
        ]);
    }
}
