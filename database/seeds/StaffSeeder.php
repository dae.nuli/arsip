<?php

use Illuminate\Database\Seeder;
use App\Models\Staff;

class StaffSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Staff::truncate();
        Staff::insert([
        	[
        		'name' => 'Nuli Giarsyani',
        		'email' => 'user@example.com',
        		'email_verified_at' => date('Y-m-d H:i:s'),
        		'password' => bcrypt(111111)
        	]
        ]);
    }
}
