<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Models\Archive;
use App\Models\Category;
use Faker\Factory as Faker;

class ArchiveSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$category = Category::pluck('id')->toArray();
    	$user = User::pluck('id')->toArray();
    	$faker = Faker::create();
        Archive::truncate();
        foreach (range(1, 5) as $key) {
	        Archive::insert([
        		'category_id' => rand(min($category), max($category)),
        		'user_id' => rand(min($user), max($user)),
        		'title' => $faker->sentence(rand(3, 5)),
        		'content' => $faker->sentence(rand(10, 20), true),
        		'files' => null,
        		'created_at' => date('Y-m-d H:i:s'),
        		'updated_at' => date('Y-m-d H:i:s')
	        ]);
        }
    }
}
