@extends('admin.layouts.app')

@section('select2-script')
  <!-- Select2 -->
  <link rel="stylesheet" href="{{ asset('AdminLTE-2.4.5/bower_components/select2/dist/css/select2.min.css') }}">
  <link href="/assets/colorbox/css/colorbox.css" rel="stylesheet">
@endsection


@section('end-script')
  <script type="text/javascript" src="/assets/colorbox/js/jquery.colorbox.js"></script>
  <script src="{{ asset('AdminLTE-2.4.5/bower_components/select2/dist/js/select2.full.min.js') }}"></script>
  <script src="{{ asset('AdminLTE-2.4.5/bower_components/jquery-form-validator/form-validator/jquery.form-validator.min.js') }}"></script>
  <script type="text/javascript" src="/packages/barryvdh/elfinder/js/standalonepopup.js"></script>
  <script src="{{ asset('AdminLTE-2.4.5/dist/js/jquery.blockUI.js') }}"></script>
  <script src="{{ asset('AdminLTE-2.4.5/dist/js/custom.js') }}"></script>
  <script type="text/javascript">
    $.validate({
        form : '.form-horizontal',
        onSuccess : function() {
          waiting();
        }
    });
    $('.select2').select2();
  </script>
@endsection

@section('content')
<div class="box">
	<div class="box-header with-border">
        <a href="{{$url}}" class="btn btn-warning"><i class="fa fa-fw fa-arrow-left"></i> Back</a>
	</div>
    {!! form_start($form, ['class' => 'form-horizontal']) !!}
	<div class="box-body">
	  	{!! form_rest($form) !!}
	</div>
	<div class="box-footer">
		<div class="col-sm-8 col-sm-offset-2">
		  	<button type="submit" class="btn btn-primary">Submit</button>
		</div>
	</div>
    {!! form_end($form) !!}
</div>
@endsection
