<div class="form-group">
  	<label for="attachment" class="col-sm-2 control-label">File</label>
  	<div class="col-sm-8">
		<a target="_blank" class="btn btn-default" href="{{url('admin/archive/download/'.$options['value']->id)}}"><i class="fa fa-cloud-download"></i> Download</a>
  	</div>
</div>