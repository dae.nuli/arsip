<div class="form-group">
  <label for="attachment" class="col-sm-2 control-label">File</label>
  <div class="col-sm-8">
    <input type="hidden" id="files" name="files" value="{{$options['value']->files}}">
    <a href="#" class="btn btn-default popup_selector" data-inputid="files">Select File</a>
    <br><br>
    @if(!empty($options['value']->files))
    <a href="{{url('admin/incoming/view/'.$options['value']->id)}}" target="_blank" class="btn btn-default btn-xs"><i class='fa fa-eye'></i> View</a>
    @endif
  </div>
</div>
