<div class="form-group">
  <label for="attachment" class="col-sm-2 control-label">File</label>
  <div class="col-sm-8">
        {{-- <input type="hidden" class="files" id="files" name="files_in"> --}}
        {{-- <a href="#" class="btn btn-primary popup_selector btn-xs" data-inputid="files"><i class='fa fa-cloud-upload'></i> Select File</a> --}}
        @if($options['value']->files)
        <a href="{{url('admin/incoming/view/'.$options['value']->id)}}" target="_blank" class="btn btn-default"><i class='fa fa-eye'></i> View File</a>
        @endif
	    {{-- <a href="" class="popup_selector btn btn-default" data-inputid="files">Select Image</a> --}}
  </div>
</div>
