@extends('admin.layouts.app')

@section('select2-script')
  <!-- Select2 -->
  <link rel="stylesheet" href="{{ asset('AdminLTE-2.4.5/bower_components/select2/dist/css/select2.min.css') }}">
@endsection

@section('head-script')

	<link rel="stylesheet" href="/AdminLTE-2.4.5/bower_components/bootstrap-daterangepicker/daterangepicker.css">
	<link rel="stylesheet" href="{{asset('AdminLTE-2.4.5/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
@endsection

@section('end-script')
	<script src="{{ asset('AdminLTE-2.4.5/bower_components/select2/dist/js/select2.full.min.js') }}"></script>
	<script src="/AdminLTE-2.4.5/bower_components/moment/min/moment.min.js"></script>
	<script src="/AdminLTE-2.4.5/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
	<script src="{{asset('AdminLTE-2.4.5/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
	<script src="{{asset('AdminLTE-2.4.5/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
	<script src="{{asset('AdminLTE-2.4.5/dist/js/custom.js')}}"></script>
	<script type="text/javascript">
		var table;
		$(function() {
		    table = $('#example2').DataTable({
		        processing: true,
		        serverSide: true,
		        ajax: '{{$ajax}}',
        		order: [[5,'desc']],
		        columns: [
		            { data: 'id', searchable: false, orderable: false},
								{ data: 'type', searchable: false, orderable: false},
								{ data: 'category_id', searchable: false, orderable: false},
		            { data: 'mail_number', searchable: true, orderable: false},
		            // { data: 'return_address', searchable: false, orderable: false},
		            { data: 'receiver', searchable: false, orderable: false},
		            { data: 'letter_entry', searchable: false, orderable: true},
		            { data: 'received_date', searchable: false, orderable: true},
		            { data: 'action', searchable: false, orderable: false}
		        ],
		        columnDefs: [{
		          "targets": 0,
		          "searchable": false,
		          "orderable": false,
		          "data": null,
		          // "title": 'No.',
		          "render": function (data, type, full, meta) {
		              return meta.settings._iDisplayStart + meta.row + 1;
		          }
		        }],
		    });
		});
	$('#reservation').daterangepicker();
	// $('.select2').select2();
	$('.select2').css('width', '100%').select2();
	</script>
@endsection

@section('content')
<div class="box">
	<div class="box-header with-border">
	  	{{-- <h3 class="box-title">Title</h3> --}}
	    <a href="{{$create}}" class="btn btn-primary"><i class="fa fa-fw fa-plus"></i> Create</a>
        <a href="#" class="btn btn-info pull-right" data-toggle="modal" data-target="#myModal"><i class="fa fa-fw fa-download"></i> Download</a>
	</div>
	<div class="box-body">
	  	<table id="example2" class="table table-bordered table-hover">
            <thead>
	            <tr>
								<th>#</th>
								<th>Jenis</th>
								<th>Kategori</th>
								<th>Nomor Surat</th>
								{{-- <th>Alamat Pengirim</th> --}}
								<th>Penerima</th>
								<th>Tanggal Masuk</th>
								<th>Tanggal Terima</th>
								<th>Action</th>
	            </tr>
            </thead>
            <tbody>
	        </tbody>
	    </table>
	</div>
</div>
<div class="modal fade" id="myModal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Filter Download</h4>
			</div>
			<div class="modal-body">
				<form action="{{url('admin/incoming/download')}}" id="form1" method="POST" class="form-horizontal">
					@csrf
	              <div class="box-body">
	                <div class="form-group">
	                  	<label class="col-sm-3 control-label">Tahun</label>
	                  	<div class="col-sm-7">
		                    <select class="form-control select2" name="year" required>
		                    	@foreach(range(2018, date('Y')) as $row)
			                    	<option value="{{$row}}" {{($row==date('Y')) ? 'selected': ''}}>{{ $row }}</option>
		                    	@endforeach
		                    </select>
	                  	</div>
	                </div>
	                <div class="form-group">
	                  	<label class="col-sm-3 control-label">Bulan</label>
	                  	<div class="col-sm-7">
												<select class="select2 form-control" name="month" required>
		                    	@for($i=1; $i <= 12; $i++)
			                    	<option value="{{$i}}" {{($i==date('n')) ? 'selected': ''}}>{{ date('F', strtotime(date('Y').'-'.$i.'-01')) }}</option>
		                    	@endfor
		                    </select>
	                    	{{-- <input class="form-control" name="letter_entry" id="reservation" required> --}}
	                  	</div>
	                </div>
{{-- 	                <div class="form-group">
	                  <label class="col-sm-2 control-label">Type</label>
	                  <div class="col-sm-10">
	                    <div class="radio">
		                    <label>
		                      <input type="radio" name="document_type" id="document_type" value="1" checked="">
		                      PDF
		                    </label>
		                </div>
	                    <div class="radio">
		                    <label>
		                      <input type="radio" name="document_type" id="document_type" value="2">
		                      Excel
		                    </label>
		                </div>
	                  </div>
	                </div> --}}
	              </div>
	              <!-- /.box-footer -->
	            </form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
				<button type="submit" form="form1" class="btn btn-primary">Download</button>
			</div>
		</div>
	<!-- /.modal-content -->
	</div>
<!-- /.modal-dialog -->
</div>
@endsection
