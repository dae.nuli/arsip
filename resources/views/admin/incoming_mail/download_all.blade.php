<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Laporan Surat</title>
    <link rel="stylesheet" href="{{asset('css/pdf.css')}}" media="all" />
  </head>
  <body>
  	<h5 class="title">PEMERINTAH KABUPATEN BANGGAI</h5>
  	<h5 class="title">DINAS PENANAMAN MODAL DAN</h5>
  	<h5 class="title">PELAYANAN TERPADU SATU PINTU (DPMPTSP)</h5>
  	<h5 class="title">SULAWESI - TENGAH</h5>
  	<hr>
  	<div>
  		<table class="table-content">
  			<thead style="background-color: darkgray;">
	  			<tr class="tr-atas">
            <th rowspan="2">NO</th>
            <th rowspan="2">Jenis</th>
            <th rowspan="2">Kategori</th>
            <th rowspan="2">Berkas</th>
            <th rowspan="2">Alamat Pengirim</th>
            <th colspan="3">Dari surat masuk</th>
            <th colspan="3">Keterangan</th>
            <th rowspan="2">Tujuan Disposisi</th>
  				</tr>
  				<tr>
	            <th>Tanggal</th>
	            <th>Nomor</th>
	            <th>Perihal</th>
	            <th>Penerima</th>
	            <th>Tanggal</th>
	            <th>Jam</th>
	         </tr>
  			</thead>
  			<tbody>
  				@php
  				$no = 1;
  				@endphp
  				@if($data)
	  				@if($type)
						@foreach($data as $row)
				        <tr>
				            <td>{{$no++}}</td>
                    <td>{{($row->type == 1) ? 'Surat Masuk' : 'Surat Keluar'}}</td>
                    @if($row->category_id)
                      <td>{{$row->category->name}}</td>
                    @else
                      <td>-</td>
                    @endif
                    <td>{{$row->code}}</td>
				            <td>{{$row->return_address}}</td>
				            <td>{{$row->letter_entry}}</td>
				            <td>{{$row->mail_number}}</td>
				            <td>{{$row->subject}}</td>
				            <td>{{$row->receiver}}</td>
				            <td>{{\Carbon\Carbon::parse($row->received_date)->format('d M Y')}}</td>
				            <td>{{\Carbon\Carbon::parse($row->received_date)->format('H:i')}}</td>
				            <td>{{$row->disposition}}</td>
				        </tr>
				        @endforeach
	  				@else
					{{-- @foreach($data as $row) --}}
			        <tr>
			            <td>{{$no++}}</td>
			            <td>{{$data->code}}</td>
			            <td>{{$data->return_address}}</td>
			            <td>{{$data->letter_entry}}</td>
			            <td>{{$data->mail_number}}</td>
			            <td>{{$data->subject}}</td>
			            <td>{{$data->receiver}}</td>
			            <td>{{\Carbon\Carbon::parse($data->received_date)->format('d M Y')}}</td>
			            <td>{{\Carbon\Carbon::parse($data->received_date)->format('H:i')}}</td>
			            <td>{{$data->disposition}}</td>
			        </tr>
			        @endif
			        {{-- @endforeach --}}
		        @else
			        <tr class="not">
			        	<td colspan="10">DATA TIDAK TERSEDIA</td>
			        </tr>
		        @endif
	    	</tbody>
  		</table>
  	</div>
  </body>
</html>
