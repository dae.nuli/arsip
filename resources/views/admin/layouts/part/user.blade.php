<li class="dropdown user user-menu">
  <a href="#" class="dropdown-toggle" data-toggle="dropdown">
    <img src="/AdminLTE-2.4.5/dist/img/user2-160x160.jpg" class="user-image" alt="User Image">
    <span class="hidden-xs">{{ Auth::user()->name }}</span>
  </a>
  <ul class="dropdown-menu">
    <!-- User image -->
    <li class="user-header">
      <img src="/AdminLTE-2.4.5/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">

      <p>
        {{ Auth::user()->name }}
        <small>Member since {{ Carbon\Carbon::parse(Auth::user()->created_at)->format('M. Y') }}</small>
      </p>
    </li>
    <!-- Menu Body -->
{{--     <li class="user-body">
      <div class="row">
        <div class="col-xs-4 text-center">
          <a href="#">Followers</a>
        </div>
        <div class="col-xs-4 text-center">
          <a href="#">Sales</a>
        </div>
        <div class="col-xs-4 text-center">
          <a href="#">Friends</a>
        </div>
      </div>
      <!-- /.row -->
    </li> --}}
    <!-- Menu Footer-->
    <li class="user-footer">
      <div class="pull-left">
        <a href="{{url('admin/profile')}}" class="btn btn-default btn-flat">Profile</a>
      </div>
      <div class="pull-right">
        <a href="{{ route('admin.logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" class="btn btn-default btn-flat">Sign out</a>
        <form id="logout-form" action="{{ route('admin.logout') }}" method="POST" style="display: none;">@csrf</form>
      </div>
    </li>
  </ul>
</li>
