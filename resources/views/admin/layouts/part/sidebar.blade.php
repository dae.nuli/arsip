<section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="/AdminLTE-2.4.5/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>{{ Auth::user()->name }}</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- search form -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
          <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <li {{($urlactive == 'home') ? 'class=active' : ''}}>
          <a href="{{url('admin/home')}}">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
          </a>
        </li>
        @if(Auth::user()->role == 'admin')
          <li class="treeview {{($urlactive == 'category') ? 'active' : ''}}">
            <a href="#">
              <i class="fa fa-files-o"></i>
              <span>Kategori</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li><a href="{{url('admin/category')}}"><i class="fa fa-circle-o"></i> List</a></li>
              <li><a href="{{url('admin/category/create')}}"><i class="fa fa-circle-o"></i> New</a></li>
            </ul>
          </li>
        @endif
        {{-- <li class="treeview {{($urlactive == 'archive') ? 'active' : ''}}">
          <a href="#">
            <i class="fa fa-book"></i>
            <span>Document</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{url('admin/archive')}}"><i class="fa fa-circle-o"></i> List</a></li>
            <li><a href="{{url('admin/archive/create')}}"><i class="fa fa-circle-o"></i> New</a></li>
          </ul>
        </li> --}}
        <li class="treeview {{($urlactive == 'incoming') ? 'active' : ''}}">
          <a href="#">
            <i class="fa fa-folder-o"></i>
            <span>Arsip</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{url('admin/incoming')}}"><i class="fa fa-circle-o"></i> List</a></li>
            <li><a href="{{url('admin/incoming/create')}}"><i class="fa fa-circle-o"></i> New</a></li>
          </ul>
        </li>
        @if(Auth::user()->role == 'admin')
          <li class="treeview {{($urlactive == 'staff') ? 'active' : ''}}">
            <a href="#">
              <i class="fa fa-users"></i> <span>Pegawai</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li><a href="{{url('admin/staff')}}"><i class="fa fa-circle-o"></i> List</a></li>
              <li><a href="{{url('admin/staff/create')}}"><i class="fa fa-circle-o"></i> New</a></li>
            </ul>
          </li>
        @endif
        {{-- <li class="treeview {{($urlactive == 'outgoing') ? 'active' : ''}}">
          <a href="#">
            <i class="fa fa-folder-open-o"></i> <span>Outcoming Mails</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{url('admin/outgoing')}}"><i class="fa fa-circle-o"></i> List</a></li>
            <li><a href="{{url('admin/outgoing/create')}}"><i class="fa fa-circle-o"></i> New</a></li>
          </ul>
        </li> --}}
        @if(Auth::user()->role == 'admin')
          <li class="treeview {{($urlactive == 'user') ? 'active' : ''}}">
            <a href="#">
              <i class="fa fa-user-secret"></i> <span>Admin</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li><a href="{{url('admin/user')}}"><i class="fa fa-circle-o"></i> List</a></li>
              <li><a href="{{url('admin/user/create')}}"><i class="fa fa-circle-o"></i> New</a></li>
            </ul>
          </li>
        @endif
{{--         <li><a href="https://adminlte.io/docs"><i class="fa fa-book"></i> <span>Documentation</span></a></li>
        <li class="header">LABELS</li>
        <li><a href="#"><i class="fa fa-circle-o text-red"></i> <span>Important</span></a></li>
        <li><a href="#"><i class="fa fa-circle-o text-yellow"></i> <span>Warning</span></a></li>
        <li><a href="#"><i class="fa fa-circle-o text-aqua"></i> <span>Information</span></a></li> --}}
      </ul>
    </section>
