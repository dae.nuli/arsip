@extends('admin.layouts.app')

@section('head-script')
  <link rel="stylesheet" href="/AdminLTE-2.4.5/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" />
  <link href="/assets/colorbox/css/colorbox.css" rel="stylesheet">
  {{-- <link rel="stylesheet" href="{{ asset('AdminLTE-2.4.5/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}"> --}}
@endsection

@section('end-script')
  <script type="text/javascript" src="/assets/colorbox/js/jquery.colorbox.js"></script>
  <script type="text/javascript" src="/packages/barryvdh/elfinder/js/standalonepopup.js"></script>
  <script src="{{ asset('AdminLTE-2.4.5/bower_components/jquery-form-validator/form-validator/jquery.form-validator.min.js') }}"></script>
  <script src="{{ asset('AdminLTE-2.4.5/dist/js/jquery.blockUI.js') }}"></script>
  <script src="{{ asset('AdminLTE-2.4.5/dist/js/custom.js') }}"></script>
@endsection

@section('content')
@if(Session::has('success'))
  <div class="alert alert-success alert-dismissible">
    {{Session::get('success')}}
  </div>
@endif
<div class="box">
	<div class="box-header with-border">
        <a href="{{$url}}" class="btn btn-warning"><i class="fa fa-fw fa-arrow-left"></i> Back</a>
	</div>
    {!! form_start($form, ['class' => 'form-horizontal']) !!}
	<div class="box-body">
	  	{!! form_rest($form) !!}
	</div>
  <div class="box-footer">
    <div class="col-sm-8 col-sm-offset-2">
        <button type="submit" class="btn btn-primary">Upload</button>
        {{-- <a href="#" class="btn btn-primary popup_selector"><i class='fa fa-cloud-upload'></i> Upload</a> --}}
    </div>
  </div>
    {!! form_end($form) !!}
</div>
@endsection