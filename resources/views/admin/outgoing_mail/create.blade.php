@extends('admin.layouts.app')

@section('head-script')
  <link rel="stylesheet" href="/AdminLTE-2.4.5/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" />
  {{-- <link rel="stylesheet" href="{{ asset('AdminLTE-2.4.5/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}"> --}}
@endsection

@section('end-script')
  <script src="/AdminLTE-2.4.5/bower_components/moment/min/moment.min.js"></script>
  <script type="text/javascript" src="/AdminLTE-2.4.5/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
  {{-- <script src="{{ asset('AdminLTE-2.4.5/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script> --}}
  <script src="{{ asset('AdminLTE-2.4.5/bower_components/jquery-form-validator/form-validator/jquery.form-validator.min.js') }}"></script>
  <script src="{{ asset('AdminLTE-2.4.5/dist/js/jquery.blockUI.js') }}"></script>
  <script src="{{ asset('AdminLTE-2.4.5/dist/js/custom.js') }}"></script>
  <script type="text/javascript">
    $.validate({
        form : '.form-horizontal',
        onSuccess : function() {
          waiting();
        }
    });
    $('.datepicker').datetimepicker({
      format: 'YYYY-MM-DD HH:mm'
    });
    // $('.datepicker').datepicker({
    //   format: 'yyyy-mm-dd',
    //   autoclose: true,
    //   minDate: 0,
    //   startDate: '-0m',
    // });
  </script>
@endsection

@section('content')
<div class="box">
	<div class="box-header with-border">
        <a href="{{$url}}" class="btn btn-warning"><i class="fa fa-fw fa-arrow-left"></i> Back</a>
	</div>
    {!! form_start($form, ['class' => 'form-horizontal']) !!}
	<div class="box-body">
	  	{!! form_rest($form) !!}
	</div>
	<div class="box-footer">
		<div class="col-sm-8 col-sm-offset-2">
		  	<button type="submit" class="btn btn-primary">Submit</button>
		</div>
	</div>
    {!! form_end($form) !!}
</div>
@endsection