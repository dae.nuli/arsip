@extends('admin.layouts.app')

@section('content')
  @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

@if(Session::has('success'))
  <div class="alert alert-success alert-dismissible">
    {{Session::get('success')}}
  </div>
@endif

<div class="box">
	<div class="box-header with-border">
	     {{-- <a href="{{$update}}" class="btn btn-primary"><i class="fa fa-fw fa-plus"></i> Edit</a> --}}
	</div>
	<div class="box-body">
    <div class="nav-tabs-custom">
      <ul class="nav nav-tabs">
        <li class="active"><a href="#activity" data-toggle="tab">Profile</a></li>
        <li><a href="#settings" data-toggle="tab">Password</a></li>
      </ul>
      <div class="tab-content">
        <div class="active tab-pane" id="activity">

          <form class="form-horizontal" method="POST" action="{{url('admin/profile/update')}}">
            @csrf
            <div class="form-group">
              <label for="inputName" class="col-sm-2 control-label">Name</label>

              <div class="col-sm-8">
                <input type="text" class="form-control" name="name" autocomplete="off" value="{{$row->name}}" id="inputName" placeholder="Name">
              </div>
            </div>
            <div class="form-group">
              <label for="inputEmail" class="col-sm-2 control-label">Email</label>

              <div class="col-sm-8">
                <input type="email" class="form-control" disabled="" value="{{$row->email}}" id="inputEmail" placeholder="Email">
              </div>
            </div>
            <div class="form-group">
              <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </div>
          </form>
        </div>
        <div class="tab-pane" id="settings">
          <form class="form-horizontal" method="POST" action="{{url('admin/profile/updatePass')}}">
            @csrf
            <div class="form-group">
              <label for="inputPassword" class="col-sm-2 control-label">Password</label>

              <div class="col-sm-8">
                <input type="password" class="form-control" autocomplete="off" name="password" id="inputPassword" placeholder="Password">
              </div>
            </div>
            <div class="form-group">
              <label for="inputPassword" class="col-sm-2 control-label">Confirm Password</label>

              <div class="col-sm-8">
                <input type="password" class="form-control" autocomplete="off" name="password_confirmation" id="inputPassword" placeholder="Confirm Password">
              </div>
            </div>
            <div class="form-group">
              <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </div>
          </form>
        </div>
        <!-- /.tab-pane -->
      </div>
      <!-- /.tab-content -->
    </div>
	</div>
</div>
@endsection
