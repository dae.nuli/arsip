@extends('layouts.app')

@section('head-script')
    <link rel="stylesheet" type="text/css" href="/themes/app-assets/vendors/css/tables/datatable/datatables.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css">
@endsection

@section('end-script')
    <script src="/themes/app-assets/vendors/js/tables/datatable/datatables.min.js"></script>
    <script type="text/javascript">
        var table;
        $(function() {
            table = $('#data-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{{$ajax}}',
                order: [[4,'desc']],
                columns: [
                    { data: 'id', searchable: false, orderable: false},
                    { data: 'type', searchable: false, orderable: false},
                    { data: 'category_id', searchable: false, orderable: false},
                    { data: 'disposition', searchable: false, orderable: false},
                    { data: 'letter_entry', searchable: false, orderable: true},
                    { data: 'action', searchable: false, orderable: false}
                ],
                columnDefs: [{
                  "targets": 0,
                  "searchable": false,
                  "orderable": false,
                  "data": null,
                  // "title": 'No.',
                  "render": function (data, type, full, meta) {
                      return meta.settings._iDisplayStart + meta.row + 1;
                  }
                }],
            });
        });
    </script>
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Archive</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <table id="data-table" class="table table-striped">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Jenis</th>
                                <th>Kategori</th>
                                <th>Tujuan Disposisi</th>
                                <th>Surat Masuk</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    <table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
