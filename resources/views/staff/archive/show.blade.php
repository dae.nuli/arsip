@extends('layouts.app')

@section('head-script')
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css">
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Archive Detail</div>

                <div class="card-body">

                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">{{ __('Jenis') }}</label>

                            <div class="col-md-6">
                                @if($row->type == 1)
                                  <input type="text" class="form-control" disabled="" value="Surat Masuk">
                                @elseif($row->type == 2)
                                  <input type="text" class="form-control" disabled="" value="Surat Keluar">
                                @else
                                  <input type="text" class="form-control" disabled="" value="-">
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">{{ __('Kategori') }}</label>

                            <div class="col-md-6">
                                <input type="text" class="form-control" disabled="" value="{{ $row->category->name }}">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">{{ __('No Berkas') }}</label>

                            <div class="col-md-6">
                                <input type="text" class="form-control" disabled="" value="{{ $row->code }}">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">{{ __('No Surat') }}</label>

                            <div class="col-md-6">
                                <input type="text" class="form-control" disabled="" value="{{ $row->mail_number }}">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">{{ __('Perihal') }}</label>

                            <div class="col-md-6">
                                <input type="text" class="form-control" disabled="" value="{{ $row->subject }}">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">{{ __('Alamat Penerima') }}</label>

                            <div class="col-md-6">
                                <input type="text" class="form-control" disabled="" value="{{ $row->return_address }}">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">{{ __('Penerima') }}</label>

                            <div class="col-md-6">
                                <input type="text" class="form-control" disabled="" value="{{ $row->receiver }}">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">{{ __('Tujuan Disposisi') }}</label>

                            <div class="col-md-6">
                                <input type="text" class="form-control" disabled="" value="{{ $row->disposition }}">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">{{ __('Tanggal Masuk') }}</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" disabled="" value="{{ date('d M Y, H:i',strtotime($row->letter_entry)) }}">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">{{ __('Tanggal Terima') }}</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" disabled="" value="{{ date('d M Y, H:i',strtotime($row->received_date)) }}">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">{{ __('File') }}</label>

                            <div class="col-md-6">
                                @if(!empty($row->files))
                                <a href="{{route('staff.archive.download', $row->id)}}" class="btn btn-success btn-sm"><i class='fa fa-download' aria-hidden='true'></i> Download</a>
                                @else
                                <a href="{{route('staff.archive.download', $row->id)}}" class="btn btn-success btn-sm disabled"><i class='fa fa-download' aria-hidden='true'></i> Download</a>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <a href="{{route('staff.archive.index')}}" class="btn btn-primary">
                                    {{ __('Back') }}
                                </a>
                            </div>
                        </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
